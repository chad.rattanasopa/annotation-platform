# annotation-platform

A platform for 
-  Image annotation : webanno
-  Text annotation  : dataturks

# install mysql and php myadmin

-  https://medium.com/@migueldoctor/run-mysql-phpmyadmin-locally-in-3-steps-using-docker-74eb735fa1fc



# Running via Docker
Quick start
If you have Docker installed, you can run WebAnno using

- docker run -it --name webanno -p8080:8080 webanno/webanno:3.5.2
The command download WebAnno from Dockerhub and starts it on port 8080. If this port is not available on your machine, you should provide another port to the -p parameter.

The logs will be printed to the console. To stop the container, press CTRL-C.

# To run the WebAnno docker in the background use

docker run -d --name webanno -p8080:8080 webanno/webanno:3.5.2
Logs are accessible by typing

docker logs webanno

# When you want to run WebAnno again later, use the command

- docker start -ai webanno
or for the background mode

- docker start webanno


# Storing data on the host
- mkdir /srv/webanno

# When you run WebAnno via Docker, you then mount this folder into the container:

- docker run -it --name webanno -v /srv/webanno:/export -p8080:8080 webanno/webanno:3.5.2




